import {AfterViewInit, Component, ViewChild} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  @ViewChild('write') write;
  ctx: CanvasRenderingContext2D;

  defSize = 2;
  defColor = '#555';
  moveFlg: boolean;

  xPoint: number;
  yPoint: number;

  src: string;


  ngAfterViewInit() {
    const canvas = this.write.nativeElement;
    this.ctx = canvas.getContext('2d');

  }

  startMouse(e: MouseEvent) {
    this.setMousePosition(e);

    this.startPoint();
  }


  moveMouse(e: MouseEvent) {
    if (e.buttons === 1) {
      this.setMousePosition(e);
      this.movePoint();
    }
  }


  startTouch(e: TouchEvent) {
    e.preventDefault();
    this.setTouchPosition(e);
    this.startPoint();
  }


  moveTouch(e: TouchEvent) {
    if (e.type === 'touchmove') {
      this.setTouchPosition(e);
      this.movePoint();
    }
  }


  endPoint() {

    if (!this.moveFlg) {
      this.ctx.lineTo(this.xPoint - 1, this.yPoint - 1);
      this.ctx.lineCap = 'round';
      this.ctx.lineWidth = this.defSize * 2;
      this.ctx.strokeStyle = this.defColor;
      this.ctx.stroke();
    }
    this.moveFlg = false;
  }

  save() {
    const canvas = this.write.nativeElement;
    this.src = canvas.toDataURL();
  }

  clear() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.clientWidth, this.ctx.canvas.clientHeight);
  }


  private startPoint() {
    this.ctx.beginPath();
    this.ctx.moveTo(this.xPoint, this.yPoint);
  }

  private movePoint() {
    this.moveFlg = true;
    this.ctx.lineTo(this.xPoint, this.yPoint);
    this.ctx.lineCap = 'round';
    this.ctx.lineWidth = this.defSize;
    this.ctx.strokeStyle = this.defColor;
    this.ctx.stroke();
  }

  private setMousePosition(e: MouseEvent) {
    this.xPoint = e.layerX;
    this.yPoint = e.layerY;
  }

  private setTouchPosition(e: TouchEvent) {
    const rect = this.write.nativeElement.getBoundingClientRect();
    this.xPoint = e.touches[0].clientX - rect.left;
    this.yPoint = e.touches[0].clientY - rect.top;
  }
}
